import maya.OpenMaya as om
import maya.cmds as mc


def get_points(name):
    """Get the lists of points of the given mesh name.

    Args:
        name (str):

    Returns:
        list: A list of three floats tuples as the vertex positions.
    """
    sellist = om.MSelectionList()
    sellist.add(name)

    dagpath = om.MDagPath()
    sellist.getDagPath(0, dagpath)
    dagpath.extendToShape()

    fnmesh = om.MFnMesh(dagpath)
    pnts = om.MPointArray()
    fnmesh.getPoints(pnts)

    return [(pnts[i].x, pnts[i].y, pnts[i].z) for i in range(pnts.length())]


def compare_float_points(p1, p2, tol):
    """Compare two points with a given tolerance.

    Args:
        p1 (tuple): A tuple of 3 float values.
        p2 (tuple): A tuple of 3 float values.
        tol (float): The maximum allowed absolute difference
            between corresponding elements in the two tuples.

    Returns:
        bool: True if all elements are within the specified tolerance,
            indicating the lists are equal within the tolerance.
            False if any element is outside the specified tolerance.
    """
    if len(p1) != 3 or len(p2) != 3:
        return False

    for i in range(3):
        if abs(p1[i] - p2[i]) > tol:
            return False

    return True


def get_target_order(geom, target, tolerance):
    """Gets a list the vertex IDs of the target mesh
    those match to the given geom.

    Args:
        name (str):
        target (str):
        tolerance (float/False):

    Returns:
        list: A list of vertex IDs of the target mesh.
    """
    result = []
    pnts = get_points(geom)
    tg_pnts = get_points(target)

    for ix in range(len(pnts)):
        for iy in range(len(tg_pnts)):
            if not tolerance:
                if pnts[ix] == tg_pnts[iy]:
                    result.append(iy)
                    break
            else:
                if compare_float_points(pnts[ix], tg_pnts[iy], tolerance):
                    result.append(iy)
                    break
        else:
            result.append(-1)

    return result


def create_blend_shape(
    geom,
    base_geom,
    target_geom,
    target_base_geom,
):
    bs = mc.createNode("idBasedBlendShape")

    mc.connectAttr("{}.og[0]".format(bs), "{}.inMesh".format(geom))
    mc.connectAttr("{}.outMesh".format(base_geom), "{}.ip[0].ig".format(bs))
    mc.connectAttr("{}.outMesh".format(target_geom), "{}.tm".format(bs))
    mc.connectAttr("{}.outMesh".format(target_base_geom), "{}.tbm".format(bs))

    mc.setAttr(
        "{}.tids".format(bs),
        get_target_order(base_geom, target_base_geom),
        type="doubleArray",
    )
